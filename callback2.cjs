/* 
	Problem 2: Write a function that will return all lists that belong to a board based on
     the boardID that is passed to it from the given data in lists.json. Then pass control 
     back to the code that called it by using a callback function.
*/
const fs = require('fs');

function listBelongToBoard(lists, boardId, callback){
    setTimeout(()=>{
        fs.readFile(lists,'utf-8',(err, data)=>{
            if(err){
                console.error(err);
            }else{
                const allfiles = JSON.parse(data);
              //  console.log(allfiles)
               callback(null, allfiles[boardId])
            }
        })
    },2000)
}
module.exports = listBelongToBoard;
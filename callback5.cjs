/* 
	Problem 5: Write a function that will use the previously written functions to get the 
    following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const fs = require('fs');


const boardInfo= require('./callback1.cjs');
const listBelongToBoard = require('./callback2.cjs');
const allCardsBelongToList = require('./callback3.cjs');

function findGivenDetails2(boards, cards,lists,Thanos){

   setTimeout(()=>{

     boardInfo(boards,Thanos,(err, data)=>{
        if(err)
        {
            console.error(err);
        }
        else
        {
          //  console.log(data);
        }

        listBelongToBoard(lists, Thanos, (err, data)=>{
            if(err)
            {
                console.error(err);
            }else
            {
             //  console.log(data);
               const listOfId = data.filter((findName)=>{
                return findName.name === 'Mind' || findName.name === 'Space';
              })
                  
                allCardsBelongToList(cards, listOfId[0].id, (err, data)=>{
                    if(err)
                    {
                        console.error(err);
                    }else
                    {
                        console.log(data);
                    }
                })

                allCardsBelongToList(cards, listOfId[1].id, (err, data)=>{
                    if(err)
                    {
                        console.error(err);
                    }else
                    {
                        console.log(data);
                    }
                })
            }
        })
    })

  },2000)

}
module.exports= findGivenDetails2;
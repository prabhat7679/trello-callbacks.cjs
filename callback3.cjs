/* 
	Problem 3: Write a function that will return all cards that belong to a particular 
    list based on the listID that is passed to it from the given data in cards.json. 
    Then pass control back to the code that called it by using a callback function.

    "qwsa221"
*/

const fs = require('fs');

function allCardsBelongToList(cards, listId, callback){
    setTimeout(()=>{
        fs.readFile(cards,'utf-8',(err, data)=>{
            if(err){
                console.error(err);
            }else{
                const allfiles = JSON.parse(data);
              //  console.log(allfiles)
               callback(null, allfiles[listId])
            }
        })
    },2000)
}
module.exports = allCardsBelongToList;
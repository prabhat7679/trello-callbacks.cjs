
/* Problem 1: Write a function that will return a particular board's information based on 
the boardID that is passed from the given list of boards in boards.json and then pass 
control back to the code that called it by using a callback function.

      Boards have ids and names
        Lists belong to boards
        Cards belong to lists

*/
const fs = require('fs');

function boardInfo(board, boardId, callback){
    setTimeout(() => {
        fs.readFile(board,'utf-8', (err, data)=>{
            if(err){
                console.error(err);
            }else{
               // console.log(' file read successfully');
                const find =(JSON.parse(data));
               
              const findIdInfo = find.filter((ID)=>{
                return ID.id === boardId;
              })

               callback(null, findIdInfo) 
            }
        })
        
    },2000);
}
module.exports= boardInfo;
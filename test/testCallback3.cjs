
const allCardsBelongToList = require("../callback3.cjs");
const path = require("path");

const cards = path.resolve("../cards.json");
const listId = "qwsa221";

allCardsBelongToList(cards, listId, (err, data) => {
  if (err) {
    console.error(err);
  } else {
    console.log(data);
  }
});


const listBelongToBoard = require("../callback2.cjs");
const path = require("path");

const lists = path.resolve("../lists.json");
const boardId = "abc122dc";

listBelongToBoard(lists, boardId, (err, data) => {
  if (err) {
    console.error(err);
  } else {
    console.log(data);
  }
});

const boardInfo= require('../callback1.cjs');
const path = require('path');

const board = path.resolve('../boards.json');
const boardId = "mcu453ed";

boardInfo(board, boardId, (error ,data)=>{
     if(error){
        console.error(error);
     }else{
        console.log(data);
     }
})